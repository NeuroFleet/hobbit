from reactor.shortcuts import *

################################################################################

import trakt.tv

@Reactor.wamp.register_middleware('media.Video.Trakt')
class Trakt(Reactor.wamp.Profiler):
    def on_init(self, details):
        pass # trakt.tv.setup(apikey='')

    ############################################################################

    @Reactor.wamp.register_topic(u'media.video.trakt.watched')
    def on_watched(self, movie):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'media.video.trakt.sync')
    def synchronize(self, query):
        resp = Trakt['sync/watched']

        self.queue.spawn(self.sync_trakt_movies, resp.movies())

    #***************************************************************************

    def sync_trakt_movies(self, watched):
        for key, show in watched.items():
            self.publish(u'media.video.trakt.watched', dict([
                (key, getattr(show,key))
                for key in ('title','year')
            ]))

################################################################################

import tvdb_api

@Reactor.wamp.register_middleware('media.Video.TVDB')
class TVDB(Reactor.wamp.Profiler):
    SHOW_INFO = """{seriesname}
Status: {status}
Network: {network}
Airs: {airs_dayofweek} at {airs_time}
First Aired: {firstaired}
Runtime: {runtime}

Rating: {rating}
Genre: {genre}
CRating: {contentrating}
Actors: {actors}

{overview}"""

    EPISODE_INFO = """{seasonnumber:>02}x{episodenumber:>02} - {episodename}
First Aired: {firstaired}
Rating: {rating}
Writer/Director: {writer}/{director}
Guest Stars: {gueststars}

{overview}"""

    def on_init(self, details):
        self.tvdb = tvdb_api.Tvdb(apikey='EB10E1EFE69B3AA5', banners=True) # heavier stuff
    ############################################################################

    @Reactor.wamp.register_method(u'media.video.tvdb.info')
    def tv_info(self, mess, args):
        """ Shows the info for the given TV show
        ie. !tv info breaking bad
        [from tvrage.com]
        """
        if not args:
            return 'Am I supposed to guess the show?...'
        show = self.tvdb[args]
        self.send(mess.getFrom(), show['banner'], message_type=mess.getType())
        return SHOW_INFO.format(**show.data)

    #***************************************************************************

    @Reactor.wamp.register_method(u'media.video.tvdb.next')
    def tv_next(self, mess, args):
        """ Shows when to expect the next episode of the given show
        ie. !tv next breaking bad
        """
        if not args:
            return 'Am I supposed to guess the show?...'
        show = self.tvdb[args]
        now = datetime.now().date()
        for snb, season in six.iteritems(show):
            for enb, episode in six.iteritems(season):
                fa = episode['firstaired']
                local_episode = copy.deepcopy(episode)
                if not fa:
                    continue
                firstaired = datetime.strptime(local_episode['firstaired'], '%Y-%m-%d').date()
                if firstaired >= now:
                    local_episode['overview'] = '[ZAPPED BY ANTI-SPOILER !]'
                    days = abs((now - firstaired).days)
                    if days == 0:
                        local_episode['firstaired'] = 'it\'s today!'
                    else:
                        local_episode['firstaired'] = 'in %d days' % days
                    local_episode['firstaired'] += ' (%s)' % str(firstaired)
                    return EPISODE_INFO.format(**local_episode)

        self.send(mess.getFrom(), show['banner'], message_type=mess.getType())
        return SHOW_INFO.format(**show.data)

    @Reactor.wamp.register_method(u'media.video.tvdb.show')
    def tv_show(self, mess, args):
        """ List the seasons of the given show
        """
        if not args:
            return 'Am I supposed to guess the show?...'
        show = self.tvdb[args]

        return 'List of the seasons:\n' + '\n'.join(['Season %s' % key for key in show.keys()])

    @Reactor.wamp.register_method(u'media.video.tvdb.season')
    def tv_season(self, mess, args):
        """ List the episodes of the season
        ie. !tv season 3 breaking bad
        """
        if len(args) < 2 or not args[0].isdigit():
            return 'the syntax is !tv season # name'

        season_number = int(args[0])
        name = ' '.join(args[1:])
        season = self.tvdb[name][season_number]
        return ('List of the episodes of season %02i:\n' % season_number) + '\n'.join(
            ['%02ix%02i [%s] %s ' % (season_number, episode_number, episode['firstaired'], episode['episodename']) for episode_number, episode in six.iteritems(season)])

    @Reactor.wamp.register_method(u'media.video.tvdb.episode')
    def tv_episode(self, mess, args):
        """ List the episodes of the season
        ie. !tv episode 01x03 breaking bad
        """
        if len(args) < 2:
            return 'the syntax is !tv episode ssxee name'
        sxe = args[0].lower()
        if  sxe.index('x') == -1:
            return 'the episode syntax is ssxee with ss = season and ee = episode'
        season_number, episode_number = args[0].split('x')
        if not season_number.isdigit() or not episode_number.isdigit():
            return 'the episode syntax is ssxee with ss = season and ee = episode'
        season_number, episode_number = int(season_number), int(episode_number)
        name = ' '.join(args[1:])
        show = self.tvdb[name]
        if season_number not in show:
            return 'invalid season number'
        season = show[season_number]
        if episode_number not in season:
            return 'invalid episode number'
        episode = season[episode_number]
        self.send(mess.getFrom(), episode['filename'], message_type=mess.getType())
        return EPISODE_INFO.format(**episode)

